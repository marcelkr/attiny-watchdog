# ATtiny24 Watchdog

This is a watchdog implementation for monitoring wether a special I2C messages is received. If this is not the case, a pin is toggled to restart the monitored device. If some consecutive restarts happen, the pin stays low to keep the monitored device shut down.

This state chart shows the basic idea of this watchdog:
![](doc/statechart.svg)

In my special case, a Raspberry Pi fish tank controller is monitored. The I2C messages were send from a Python script running on the Pi.

## The I2C message
The I2C message consists of two bytes. The first byte denotes the desired function, the second byte is the payload.

| Function   | Data                                                 |
|------------|------------------------------------------------------|
| `0x00`     | `value*10` is set to be the new watchdog timer value |
| `0x01`     | `0`: Buzzer off, `1`: Buzzer on                      |
| `0x02`     | `0`: Red LED off, `1`: LED on                        |
| `0x03`     | `0`: Green LED off, `1`: LED on                      |
| `0x04`     | `0`: Blue LED off, `1`: LED on                       |
