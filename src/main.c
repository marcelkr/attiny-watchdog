#include <stdint.h>
#include <stdbool.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "USI_TWI_Slave.h"

/* ### Configuration ################### */
#define WDG_STARTUP_TIME 150
#define RESTART_TIME 3
#define MAX_CONSECUTIVE_RESTARTS 5
#define I2C_ADDRESS ((uint8_t)0x10)


/* ### Helper Macros ################### */
#define LED_DDR DDRA
#define LED_PORT PORTA
#define LED_RED PA0
#define LED_GREEN PA1
#define LED_BLUE PA2
#define LED_TOGGLE(x) (LED_PORT ^= (1<<x))
#define LED_ON(x) (LED_PORT &= ~(1<<x))
#define LED_OFF(x) (LED_PORT |= (1<<x))

#define BUZZER_DDR DDRA
#define BUZZER_PORT PORTA
#define BUZZER PA3
#define BUZZER_ON (BUZZER_PORT |= (1<<BUZZER))
#define BUZZER_OFF (BUZZER_PORT &= ~(1<<BUZZER))

#define EN_DDR DDRA
#define EN_PORT PORTA
#define EN_ON (EN_PORT |= (1<<PA7))
#define EN_OFF (EN_PORT &= ~(1<<PA7))


/* ### Typedefs ################### */
typedef enum {
    STARTUP,
    RUNNING,
    RESTART,
    SHUTDOWN
} state_t;


/* ### Prototypes ################### */
void init(void);
void led_set(uint8_t led, bool state);
void leds_set(bool red, bool green, bool blue);
bool i2c_get_command(uint8_t* function, uint8_t* data);
void process_i2c_command(uint8_t function, uint8_t data);


/* ### Global variables */
static volatile uint16_t wdg_timer;
static volatile uint8_t restart_timer;


/* ### Implementation ################### */
ISR(TIM1_COMPA_vect) {
    if (wdg_timer > 0) {
        wdg_timer--;
    }
    if (restart_timer > 0) {
        restart_timer--;
    }
}

void init(void) {
    EN_DDR |= (1<<PA7);

    BUZZER_DDR |= (1<<BUZZER);
    BUZZER_OFF;

    LED_DDR |= (1 << LED_BLUE) | (1 << LED_GREEN) | (1 << LED_RED);
    LED_PORT |= (1 << LED_BLUE) | (1 << LED_GREEN) | (1 << LED_RED);

    USI_TWI_Slave_Initialise(I2C_ADDRESS);

    // Init timer
    TCCR1B |= (1<<CS10) | (1<<CS11); // Prescaler = 64
    TCCR1B |= (1<<WGM12);   // Waveform generation mode 4: Clear on compare to OCR1A
    TIMSK1 |= (1<<OCIE1A);  // Enable output compare match interrupt
    OCR1A = 15625;          // Compare value 15625 => Interrupts every 1 s @ 1 MHz clock

    sei();
}

void leds_set(const bool red, const bool green, const bool blue) {
    if (red) {
        LED_ON(LED_RED);
    } else {
        LED_OFF(LED_RED);
    }

    if (green) {
        LED_ON(LED_GREEN);
    } else {
        LED_OFF(LED_GREEN);
    }

    if (blue) {
        LED_ON(LED_BLUE);
    } else {
        LED_OFF(LED_BLUE);
    }
}

void led_set(const uint8_t led, const bool state) {
    if (state) {
        LED_ON(led);
    } else {
        LED_OFF(led);
    }
}

bool i2c_get_command(uint8_t* function, uint8_t* data) {
    if (USI_TWI_Word_In_Receive_Buffer()) {
        *function = USI_TWI_Receive_Byte();
        *data = USI_TWI_Receive_Byte();
        return true;
    } else {
        return false;
    }
}

void process_i2c_command(const uint8_t function, const uint8_t data) {
    switch (function) {
        case 0x00:
            wdg_timer = data * 10;
            break;
        case 0x01:
            if (data == 0) {
                BUZZER_OFF;
            } else {
                BUZZER_ON;
            }
            break;
        case 0x02:
            led_set(LED_RED, (data == 1 ? true : false));
            break;
        case 0x03:
            led_set(LED_GREEN, (data == 1 ? true : false));
            break;
        case 0x04:
            led_set(LED_BLUE, (data == 1 ? true : false));
            break;
    }
}

int main(void) {
    init();

    while (1) {
        static state_t state = STARTUP;
        static state_t state_old = RUNNING;
        static bool i2c_has_command = false;
        static uint8_t consecutive_restarts = 0;
        uint8_t i2c_function;
        uint8_t i2c_data;

        i2c_has_command = i2c_get_command(&i2c_function, &i2c_data);

        switch(state) {
            case STARTUP:
                if (state != state_old) {
                    leds_set(true, true, false);
                    wdg_timer = WDG_STARTUP_TIME;
                    EN_ON;
                    state_old = state;
                }
                if (i2c_has_command && i2c_function == 0x00 && i2c_data > 0) {
                    wdg_timer = i2c_data * 10;
                    consecutive_restarts = 0;
                    state = RUNNING;
                }
                if (wdg_timer == 0) {
                    consecutive_restarts++;
                    if (consecutive_restarts >= MAX_CONSECUTIVE_RESTARTS) {
                        state = SHUTDOWN;
                    } else {
                        state = RESTART;
                    }
                }
                break;

            case RUNNING:
                if (state != state_old) {
                    leds_set(false, true, false);
                    state_old = state;
                }
                if (i2c_has_command) {
                    process_i2c_command(i2c_function, i2c_data);
                }
                if (wdg_timer == 0) {
                    state = RESTART;
                }
                break;

            case RESTART:
                if (state != state_old) {
                    restart_timer = RESTART_TIME;
                    leds_set(true, false, false);
                    state_old = state;
                    EN_OFF;
                }
                if (restart_timer == 0) {
                    state = STARTUP;
                }
                break;

            case SHUTDOWN:
                if (state != state_old) {
                    leds_set(true, false, false);
                    state_old = state;
                    EN_OFF;
                }
                if (wdg_timer == 0) {
                    BUZZER_ON;
                    _delay_ms(100);
                    BUZZER_OFF;
                    _delay_ms(100);
                    BUZZER_ON;
                    _delay_ms(100);
                    BUZZER_OFF;
                    _delay_ms(100);
                    BUZZER_ON;
                    _delay_ms(100);
                    BUZZER_OFF;
                    _delay_ms(100);
                    BUZZER_ON;
                    _delay_ms(100);
                    BUZZER_OFF;
                    _delay_ms(100);
                    BUZZER_ON;
                    _delay_ms(100);
                    BUZZER_OFF;
                    _delay_ms(100);
                    wdg_timer = 600;
                }
                LED_ON(LED_RED);
                _delay_ms(100);
                LED_OFF(LED_RED);
                _delay_ms(100);
                break;
        }

        i2c_has_command = false;
    }

    return 0;
}